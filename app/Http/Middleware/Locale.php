<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Make sure current locale exists.
        $locale = $request->segment(1);

        if ( ! array_key_exists($locale, config('app.locales'))) {
            $segments = $request->segments();
            $segments[0] = config('app.locale');

            return redirect()->to(implode('/', $segments));
        }

        App::setLocale($locale);

        return $next($request);
    }
}
