<?php

namespace App\Http\Controllers;

use App\Event;
use App\Guess;
use App\Match;
use App\Phase;
use App\Result;
use App\Team;
use Illuminate\Http\Request;

class GuessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::all()->first();
        if($user = $request->user()) {
            Guess::where('user_id', $request->user()->id)->forceDelete();
            foreach(Match::all() as $match){
                $name = str_replace(' ', '_', $match->name);
                if($request->has($name)){
                    foreach($request->get($name) as $guess){
                        Guess::create([
                            'user_id' => $request->user()->id,
                            'team_id' => Team::where('code', $guess)->first()->id,
                            'match_id' => Match::where('name', $match->name)->first()->id,
                            'event_id' => $event->id,
                            'is_result' => $user->is_admin ? true : false,
                        ]);
                    }
                }
            }
            $request->user()->events()->sync([
                $event->id => [
                    'moved_teams' => $request->get('moved-teams'),
                    'locked_teams' => $request->get('locked-teams'),
                ]
            ]);
            $message = $user->is_admin ? __('Saved result') : __('Saved guess');
        }
        return redirect()->back()->with(
            ['message' => $message]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
