<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guess extends Model
{
    protected $fillable = [
        'team_id',
        'goals',
        'phase_id',
        'user_id',
        'match_id',
        'event_id',
        'is_result',
    ];

    public function team() {
        return $this->belongsTo(Team::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function match() {
        return $this->belongsTo(Match::class);
    }

    public function event() {
        return $this->belongsTo(Event::class);
    }
}
