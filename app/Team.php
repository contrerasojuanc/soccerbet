<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Team extends Model
{
    protected $appends = [
        'image',
    ];

    public function getImageAttribute($value){
        $file = 'img/' . $this->code . '.png';

        return file_exists(public_path() . '/' . $file) ? asset($file) : asset('img/Custom.png');
    }

    public function getNameAttribute($value){
        return __($this->attributes['code']);
    }

    public function groups() {
        return $this->belongsToMany(Group::class);
    }
}
