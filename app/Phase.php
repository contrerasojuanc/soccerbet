<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{

    public function groups() {
        return $this->hasMany(Group::class);
    }

    public function matches() {
        return $this->hasMany(Match::class);
    }
}
