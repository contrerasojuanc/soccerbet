<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name',
        'teams_number',
    ];

    public function teams() {
        return $this->belongsToMany(Team::class);
    }

    public function phase() {
        return $this->belongsTo(Phase::class);
    }
}
