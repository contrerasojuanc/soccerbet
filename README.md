Soccerbet - JuanContreras
========================

Allows users make guesses about soccer events

Deploy
------------
To test this app without doing any installation, please go to this link: 
```bash
https://soccerbet.herokuapp.com/es/
```

Installation
------------

First, execute next command:

```bash
$ composer install
```

Second, set local mysql database connection string (e.g. mysql://root:123456@127.0.0.1:3306/soccerbetdb) on .env file

Third, create the database and execute next commands:
```bash
$ php artisan migrate --seed
```

For upload to heroku it was needed to make this steps in order to get dependencies on node_modules
```
heroku config:set NPM_CONFIG_PRODUCTION=false
git commit --allow-empty -m "Get dependencies"
git push heroku master
heroku config:set NPM_CONFIG_PRODUCTION=true
```

Usage
-----

There is no need to configure a virtual host in your web server to access the application.
Just use the built-in web server:

```bash
$ php artisan serve
```

This command will start a web server for the Laravel application. Now you can
access the application in your browser at <http://127.0.0.1:8000>. You can
stop the built-in web server by pressing `Ctrl + C` while you're in the
terminal.

Now, access to the app using user:admin@admin.com and password:123456