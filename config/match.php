<?php

return [ 'matches' => [

    /*
    |--------------------------------------------------------------------------
    | Parent places for matches
    |--------------------------------------------------------------------------
    |
    | On changes the seeding needs to run again
    */

    //Eighths
        '1A - 2B' => [
            'name' => '1A - 2B',
            'parent_a' => 'Group A',
            'parent_b' => 'Group B',
            'phase' => 'Eighths',
        ],

        '1C - 2D' => [
            'name' => '1C - 2D',
            'parent_a' => 'Group C',
            'parent_b' => 'Group D',
            'phase' => 'Eighths',
        ],

        '1B - 2A' => [
            'name' => '1B - 2A',
            'parent_a' => 'Group B',
            'parent_b' => 'Group A',
            'phase' => 'Eighths',
        ],

        '1D - 2C' => [
            'name' => '1D - 2C',
            'parent_a' => 'Group D',
            'parent_b' => 'Group C',
            'phase' => 'Eighths',
        ],

        '1E - 2F' => [
            'name' => '1E - 2F',
            'parent_a' => 'Group E',
            'parent_b' => 'Group F',
            'phase' => 'Eighths',
        ],

        '1G - 2H' => [
            'name' => '1G - 2H',
            'parent_a' => 'Group G',
            'parent_b' => 'Group H',
            'phase' => 'Eighths',
        ],

        '1F - 2E' => [
            'name' => '1F - 2E',
            'parent_a' => 'Group F',
            'parent_b' => 'Group E',
            'phase' => 'Eighths',
        ],

        '1H - 2G' => [
            'name' => '1H - 2G',
            'parent_a' => 'Group H',
            'parent_b' => 'Group G',
            'phase' => 'Eighths',
        ],

    //Fourths
        'Fourths 1' => [
            'name' => 'Fourths 1',
            'parent_a' => '1A - 2B',
            'parent_b' => '1C - 2D',
            'phase' => 'Fourths',
        ],

        'Fourths 2' => [
            'name' => 'Fourths 2',
            'parent_a' => '1B - 2A',
            'parent_b' => '1D - 2C',
            'phase' => 'Fourths',
        ],

        'Fourths 3' => [
            'name' => 'Fourths 3',
            'parent_a' => '1E - 2F',
            'parent_b' => '1G - 2H',
            'phase' => 'Fourths',
        ],

        'Fourths 4' => [
            'name' => 'Fourths 4',
            'parent_a' => '1F - 2E',
            'parent_b' => '1H - 2G',
            'phase' => 'Fourths',
        ],

    //Semi
        'Semifinal 1' => [
            'name' => 'Semifinal 1',
            'parent_a' => 'Fourths 1',
            'parent_b' => 'Fourths 2',
            'phase' => 'Semifinal',
        ],

        'Semifinal 2' => [
            'name' => 'Semifinal 2',
            'parent_a' => 'Fourths 3',
            'parent_b' => 'Fourths 4',
            'phase' => 'Semifinal',
        ],

    //Final
        'Final' => [
            'name' => 'Final',
            'parent_a' => 'Semifinal 1',
            'parent_b' => 'Semifinal 2',
            'phase' => 'Final',
        ],
    //Third
        'Third and Fourth' => [
            'name' => 'Third and Fourth',
            'parent_a' => 'Semifinal 1',
            'parent_b' => 'Semifinal 2',
            'phase' => 'Final',
        ],

    //Champion
        'Champion' => [
            'name' => 'Champion',
            'parent_a' => 'Final',
            'phase' => 'Champion',
        ],

    //Sub-Champion
        'Sub-Champion' => [
            'name' => 'Sub-Champion',
            'parent_a' => 'Final',
            'phase' => 'Champion',
        ],

    //Third
        'Third' => [
            'name' => 'Third',
            'parent_a' => 'Third and Fourth',
            'phase' => 'Third',
        ],

    //Fourth
        'Fourth' => [
            'name' => 'Fourth',
            'parent_a' => 'Third and Fourth',
            'phase' => 'Third',
        ],

    ],

];
