<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Qualified teams by Groups
    |--------------------------------------------------------------------------
    |
    | On changes the seeding needs to run again
    */

    'a' => [
        'RU', //Rusia
        'SA', //Arabia Saudita
        'EG', //Egipto
        'UY', //Uruguay
    ],

    'b' => [
        'PT', //Portugal
        'ES', //España
        'MA', //Marruecos
        'IR', //Iran
    ],

    'c' => [
        'FR', //Francia
        'AU', //Australia
        'PE', //Peru
        'DK', //Dinamarca
    ],

    'd' => [
        'AR', //Argentina
        'IS', //Islandia
        'HR', //Croacia
        'NG', //Nigeria
    ],

    'e' => [
        'BR', //Brasil
        'CH', //Suiza
        'CR', //Costa Rica
        'RS', //Servia
    ],

    'f' => [
        'DE', //Alemania
        'MX', //Mexico
        'SE', //Suecia
        'KR', //Korea del Sur
    ],

    'g' => [
        'BE', //Belgica
        'PA', //Panama
        'TN', //Tunez
        'EL', //Inglaterra
    ],

    'h' => [
        'PL', //Polonia
        'SN', //Senegal
        'CO', //Colombia
        'JP', //Japon
    ],

];
