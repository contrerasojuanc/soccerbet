<?php

use App\Phase;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PhasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Phase::create([
            'name' => 'Classifieds',
            'teams_number' => 32,
            'points' => 0,
            'points_all_match' => 0,
        ]);

        Phase::create([
            'name' => 'Eighths',
            'teams_number' => 16,
            'points' => 1,
            'points_all_match' => 4,
            'parent' => 'Classifieds',
        ]);

        Phase::create([
            'name' => 'Fourths',
            'teams_number' => 8,
            'points' => 2,
            'points_all_match' => 4,
            'parent' => 'Eighths',
        ]);

        Phase::create([
            'name' => 'Semifinal',
            'teams_number' => 4,
            'points' => 4,
            'points_all_match' => 4,
            'parent' => 'Fourths',
        ]);

        Phase::create([
            'name' => 'Final',
            'teams_number' => 2,
            'points' => 8,
            'points_all_match' => 4,
            'parent' => 'Semifinal',
        ]);

        Phase::create([
            'name' => 'Champion',
            'teams_number' => 1,
            'points' => 25,
            'points_all_match' => 0,
            'parent' => 'Final',
        ]);

        Phase::create([
            'name' => 'Third',
            'teams_number' => 1,
            'points' => 4,
            'points_all_match' => 0,
            'parent' => 'Third and Fourth',
        ]);

    }
}
