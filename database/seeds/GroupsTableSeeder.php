<?php

use App\Group;
use App\Phase;
use App\Team;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $index = 0;
        $teams = config('team.codes');
        shuffle($teams);
        foreach (['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'] as $groupLetter) {
            $group = Group::create([
                'name' => 'Group ' . $groupLetter,
                'phase_id' => Phase::where('name','Classifieds')->first()->id,
            ]);

            $group = Group::find($group->id);
            for($teamIndex = 0 ; $teamIndex < $group->teams_number ; $teamIndex++) {
                $team = config('group.' . strtolower($groupLetter))[$teamIndex] ?? $teams[$index]['code'];
                $group->teams()->attach(Team::where('code', $team)->first()->id, ['position' => $teamIndex]);
                $index++;
            }

        }

    }
}
