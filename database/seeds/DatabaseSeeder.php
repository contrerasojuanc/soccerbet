<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(PhasesTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(MatchesTableSeeder::class);
    }
}
