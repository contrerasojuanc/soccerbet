<?php

use App\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::create([
            'name' => 'Rusia 2018',
            'type' => 'Copa Mundial',
            'start_date' => Carbon::createFromDate(2018, 6, 14),
            'end_date' => Carbon::createFromDate(2018, 7, 15),
        ]);
    }
}
