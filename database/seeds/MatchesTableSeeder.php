<?php

use App\Match;
use App\Phase;
use Illuminate\Database\Seeder;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $matches = config('match.matches');
        foreach ($matches as $configMatch) {
            $match = Match::create([
                'name' => $configMatch['name'],
                'parent_a' => $configMatch['parent_a'] ?? '',
                'parent_b' => $configMatch['parent_b'] ?? '',
                'phase_id' => Phase::where('name',$configMatch['phase'])->first()->id,
            ]);

        }
    }
}
