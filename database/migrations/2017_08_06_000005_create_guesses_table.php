<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuessesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'guesses';

    /**
     * Run the migrations.
     * @table guesses
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('team_id')->unsigned()->nullable();
            $table->integer('goals')->nullable();
            $table->integer('event_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('is_result')->defualt(false)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('team_id')
                ->references('id')->on('teams')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('match_id')
                ->references('id')->on('matches')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
