<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'matches';

    /**
     * Run the migrations.
     * @table matches
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->dateTime('datetime')->nullable();
            $table->integer('teams_number')->default(2)->nullable();
            $table->string('parent_a')->nullable();
            $table->string('parent_b')->nullable();
            $table->integer('phase_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('phase_id')
                ->references('id')->on('phases')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
