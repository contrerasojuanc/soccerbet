$(document).on("mouseenter", ".clossable", function() {
    $(this).find('.close').animate({opacity:1},100);
});

$(document).on("mouseleave", ".clossable", function() {
    $(this).find('.close').animate({opacity:0},100);
});