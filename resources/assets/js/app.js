
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./bootapp');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.prototype.bus = new Vue({
    data: function() {
        return {
            movedTeams: [],
            lockedTeams: []
        }
    }
}); // Global event bus

Vue.component('event-user', require('./components/eventuser.vue'));
Vue.component('draggable-teams', require('./components/draggable.vue'));

const app = new Vue({
    el: '#app',
});
