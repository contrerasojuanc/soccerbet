@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <draggable-teams teams="{{ \App\Team::take(32)->get()->toJson() }}"></draggable-teams>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection