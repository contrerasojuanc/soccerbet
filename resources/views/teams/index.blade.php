@extends('layouts.app')

@section('register')
    @if(\Auth::check() && \Auth::user()->is_admin)
        <li><a href="{{ route('register') }}">{{ __('Register User') }}</a></li>
    @endif
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('guesses.store') }}" method="POST">
                    {{ csrf_field() }}
                    <event-user @if($eventUser) locked-teams="{{ $eventUser->locked_teams }}" moved-teams="{{ $eventUser->moved_teams }}" @endif></event-user>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-left"><h4>{{ __('Select teams') }}</h4></div>
                                    @if(\Auth::check())
                                        @if(\Auth::user()->is_admin)
                                            <div class="pull-right left-margin"><input class="btn btn-primary" type="submit" name="submit" value="{{ __('Save Result') }}"></div>
                                            <div class="pull-right left-margin">
                                                <a class="btn btn-primary" href="{{ route('register') }}">{{ __('Register User') }}</a>
                                            </div>
                                        @else
                                            <div class="pull-right left-margin"><input class="btn btn-primary" type="submit" name="submit" value="{{ __('Save guess') }}"></div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="background:url({{ asset('img/logom.png') }}); background-size: 100% auto; background-repeat:no-repeat; background-position:center;">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <div class="row">
                                @foreach($phases as $phase)
                                    <div class="col-md-12">
                                        <span class="text-center"><h1>{{ __($phase->name) }}</h1></span>
                                    </div>
                                    @if($phase->groups()->exists())
                                        <div class="col-md-12">
                                            <div class="groups">
                                                <div class="row">
                                                    @foreach($phase->groups as $group)
                                                        <div class="col-md-4 valign-top">
                                                            <span class="text-center"><h2>{{ title_case(__(strstr($group->name, ' ', true) )) . ' ' . strstr($group->name, ' ')  }}</h2></span>
                                                            <draggable-teams
                                                                    group="{{ $group->name }}"
                                                                    parent-group="{{ json_encode([]) }}"
                                                                    teams="{{ json_encode( $group->teams ) }}"
                                                                    limit="{{ $group->teams_number }}"
                                                                    is-deletable="{{ false }}"
                                                            ></draggable-teams>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($phase->matches()->exists())
                                            <div class="col-md-12">
                                                <div class="@if($phase->matches()->count() > 2) groups @endif">
                                                    <div class="row">
                                                        @foreach($phase->matches as $key => $match)
                                                            @php
                                                                $matchArray = [];
                                                                $guesses = \Auth::check() ? \Auth::user()->guesses : [];
                                                            @endphp
                                                            @foreach($guesses as $guess)
                                                                @if($guess->match == $match && $guess->event == $event)
                                                                    @php
                                                                        $matchArray[] = $guess->team;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            <div class="col-md-4 valign-top @if($phase->matches()->count() == 2 && $loop->index == 0) col-md-offset-2 @endif">
                                                                <span class="text-center"><h2>{{ $phase->name == 'Fourths' || $phase->name == 'Semifinals' ? title_case(__(strstr($match->name, ' ', true) )) . ' ' . strstr($match->name, ' ') : __($match->name) }}</h2></span>
                                                                <draggable-teams
                                                                        ref="{{ $match->name }}"
                                                                        group="{{ $match->name }}"
                                                                        parent-group="{{ json_encode([$match->parent_a, $match->parent_b]) }}"
                                                                        teams="{{ json_encode( $matchArray ) }}"
                                                                        limit="{{ $phase->teams_number }}"
                                                                        is-deletable="{{ true }}"
                                                                ></draggable-teams>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection